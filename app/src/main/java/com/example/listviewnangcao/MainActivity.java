package com.example.listviewnangcao;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listViewTC;
    ArrayList<TraiCay> arrayTC;
    TraiCayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnhXa();

        adapter = new TraiCayAdapter(this,R.layout.dong_traicay,arrayTC);
        listViewTC.setAdapter(adapter);
    }
    private void AnhXa()
    {
        listViewTC = (ListView) findViewById(R.id.listViewTC);
        arrayTC =new ArrayList<>();

        arrayTC.add(new TraiCay("Thanh Long","Thanh long đỏ",R.drawable.thanhlong));
        arrayTC.add(new TraiCay("Thơm","Thơm quá Thơm",R.drawable.thom));
        arrayTC.add(new TraiCay("Trai Le","Trai le hinh phật",R.drawable.traile));
        arrayTC.add(new TraiCay("Xoai","Xoai chua",R.drawable.xoai));
        arrayTC.add(new TraiCay("KiWi","Kiwi chuaa",R.drawable.thanhlong));
        arrayTC.add(new TraiCay("Mận","Mận an phước",R.drawable.thom));
        arrayTC.add(new TraiCay("LEEEE","Trai le hinh phật",R.drawable.traile));

    }

}